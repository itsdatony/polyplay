// The MIT License (MIT)
//
// Copyright (c) 2019-2020 Kyle Hawk
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package com.polyplay.sweep;

import java.util.Comparator;
import java.util.TreeSet;
import java.util.stream.Collectors;

import com.polyplay.AnnotatedSegment;
import com.polyplay.Point;

public class StatusStack {
	public SweepEvent getPrevious(SweepEvent e) {
		return stack.lower(e);
	}

	public SweepEvent getNext(SweepEvent e) {
		return stack.higher(e);
	}

	public void add(SweepEvent e) {
		stack.add(e);
	}

	public void remove(SweepEvent e) {
		stack.remove(e);
	}

	private static class StatusStackComparator implements Comparator<SweepEvent> {
		@Override
		public int compare(SweepEvent a, SweepEvent b) {
			AnnotatedSegment segmentA = a.getSegment();
			AnnotatedSegment segmentB = b.getSegment();
			if (segmentA == segmentB) {
				return 0;
			}
			if (Point.areCollinear(segmentA.getStart(), segmentB.getStart(), segmentB.getEnd())) {
				if (Point.areCollinear(segmentA.getEnd(), segmentB.getStart(), segmentB.getEnd())) {
					int compare = segmentA.getStart().compareTo(segmentB.getStart());
					if (compare != 0) {
						return -compare;
					}
					compare = segmentA.getEnd().compareTo(segmentB.getEnd());
					if (compare != 0) {
						return -compare;
					}
					return a.hashCode() < b.hashCode()? -1 : 1;
				}
				return segmentB.isAboveOrOn(segmentA.getEnd()) ? -1 : 1;
			}

			return segmentB.isAboveOrOn(segmentA.getStart()) ? -1 : 1;
		}
	}

	@Override
	public String toString() {
		return stack.stream().map(e -> e.toString()).collect(Collectors.joining("\n"));
	}

	private TreeSet<SweepEvent> stack = new TreeSet<SweepEvent>(new StatusStackComparator());
}
