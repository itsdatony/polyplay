// The MIT License (MIT)
//
// Copyright (c) 2019-2020 Kyle Hawk
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package com.polyplay.sweep;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.stream.Collectors;

import com.polyplay.AnnotatedSegment;
import com.polyplay.Point;

public class SweepEventQueue {
	public SweepEvent addSegment(AnnotatedSegment segment, boolean isPrimary) {
		SweepEvent start = new SweepEvent(segment, true, isPrimary);
		SweepEvent end = new SweepEvent(segment, false, isPrimary);
		start.setPairedEvent(end);
		end.setPairedEvent(start);
		queue.add(start);
		queue.add(end);
		return start;
	}

	public boolean isEmpty() {
		return queue.isEmpty();
	}

	public SweepEvent popHead() {
		return queue.poll();//queue.pollFirst();
	}

	public SweepEvent peekHead() {
		return queue.peek();//queue.first();
	}

	public void divideStartEventSegment(SweepEvent startEvent, Point cutPoint) {
		AnnotatedSegment startSegment = startEvent.getSegment();
		boolean isPrimary = startEvent.isPrimary();

		AnnotatedSegment newSegment = new AnnotatedSegment(startSegment);
		newSegment.setStart(cutPoint);

		queue.remove(startEvent.getPairedEvent());
		startSegment.setEnd(cutPoint);
		SweepEvent newEnd = new SweepEvent(startSegment, false, isPrimary);
		startEvent.setPairedEvent(newEnd);
		newEnd.setPairedEvent(startEvent);
		queue.add(newEnd);

		addSegment(newSegment, isPrimary);
	}

	public void removePairedEvents(SweepEvent event) {
		queue.remove(event);
		SweepEvent pairedEvent = event.getPairedEvent();
		if (pairedEvent != null) {
			queue.remove(pairedEvent);
		}
	}

	private static class EventQueueComparator implements Comparator<SweepEvent> {
		@Override
		public int compare(SweepEvent a, SweepEvent b) {
			int pointCompare = a.getPoint().compareTo(b.getPoint());
			if (pointCompare != 0) {
				return pointCompare;
			}

			Point pairedPoint = a.getPairedPoint();
			pointCompare = pairedPoint.compareTo(b.getPairedPoint());
			if (pointCompare == 0) {
				return 0;
			}

			if (a.isStart() != b.isStart()) {
				return a.isStart() ? 1 : -1;
			}

			return b.getSegment().isAboveOrOn(pairedPoint) ? 1 : -1;
		}
	}

	@Override
	public String toString() {
		return queue.stream().map(e -> e.toString()).collect(Collectors.joining("\n"));
	}

	private PriorityQueue<SweepEvent> queue = new PriorityQueue<SweepEvent>(new EventQueueComparator());
}
