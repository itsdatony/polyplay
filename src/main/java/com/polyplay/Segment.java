// The MIT License (MIT)
//
// Copyright (c) 2019-2020 Kyle Hawk
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package com.polyplay;

public class Segment {
	public Segment(Point p1, Point p2) {
		if (p1.compareTo(p2) < 0) {
			start = p1;
			end = p2;
		} else {
			start = p2;
			end = p1;
		}
	}

	public Point getStart() {
		return start;
	}

	public Point getEnd() {
		return end;
	}

	public void setStart(Point start) {
		this.start = start;
	}

	public void setEnd(Point end) {
		this.end = end;
	}

	public boolean isInBounds(Point p) {
		// This returns true if p is between the two lines tangent to the segment that
		// intersect the two end points of the segment. If p is collinear with the
		// segment, then this returns true if p is actually on the segment.

		Vector2d startToP = new Vector2d(start, p);
		Vector2d startToEnd = new Vector2d(start, end);

		double dot = startToP.dot(startToEnd);
		// if `dot` is 0, then `p` is `start` or `start` == `end` (reject)
		// if `dot` is less than 0, then `p` is to the left of `start` (reject)
		if (dot < Config.epsilon) {
			return false;
		}

		double squaredLength = startToEnd.squaredNorm();
		// if `dot` > `squaredLength`, then `p` is to the right of `end` (reject)
		// therefore, if `squaredLength - dot` is less than 0, then `p` is to the
		// right of `end` (reject)
		if (squaredLength - dot < Config.epsilon) {
			return false;
		}

		return true;
	}

	public boolean isAboveOrOn(Point p) {
		return (end.x() - start.x()) * (p.y() - start.y())
				- (end.y() - start.y()) * (p.x() - start.x()) >= -Config.epsilon;
	}

	private Point start, end;
}
