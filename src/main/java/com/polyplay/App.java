// The MIT License (MIT)
//
// Copyright (c) 2019-2020 Kyle Hawk
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package com.polyplay;

import java.util.ArrayList;

import com.polyplay.AnnotatedSegment.FillInfo;
import com.polyplay.chain.SegmentChainer;
import com.polyplay.sweep.SegmentAnnotater;

//Java Program to create a polygon with a given set of vertices 
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.StrokeLineCap;
import javafx.stage.Stage;

public class App extends Application {
	static Color lineColors[] = { Color.YELLOW, Color.RED, Color.GREEN, Color.PURPLE };

	// launch the application
	public void start(Stage stage) {
		// set title for the stage
		stage.setTitle("creating polygon");

		Pane group = new Pane();
		group.setScaleY(-1.0);
		group.setBackground(new Background(new BackgroundFill(Color.BLACK, null, null)));

		double boxCoordinates[] = { 100, 100, 100, 50, 50, 50, 50, 100 };
		double boxWithHoleCoordinates[] = { 100, 100, 100, 50, 50, 50, 50, 100, 75, 100, 75, 90, 60, 90, 60, 60, 90, 60,
				90, 90, 75, 90, 75, 100 };
		double slantyCoordinates[] = { 100, 100, 100, 50, 50, 50, 150, 100 };

		double primaryCoordinates[] = boxWithHoleCoordinates;
		double secondaryCoordinates[] = slantyCoordinates;

		visualizePolygon(group, Color.ORANGE, primaryCoordinates);
		visualizePolygon(group, Color.BLUE, secondaryCoordinates);

		SegmentSet primary = SegmentAnnotater.getSegments(buildSegmentSet(primaryCoordinates), null);
		visualizeSegmentSet(group, primary, new Vector2d(0, 100));
		SegmentSet secondary = SegmentAnnotater.getSegments(buildSegmentSet(secondaryCoordinates), null);
		visualizeSegmentSet(group, secondary, new Vector2d(150, 100));
		SegmentSet combined = SegmentAnnotater.getSegments(primary, secondary);
		visualizeSegmentSet(group, combined, new Vector2d(150, 0));

		SegmentSet union = SegmentFilter.union(combined);
		SegmentSet intersection = SegmentFilter.intersect(combined);
		SegmentSet difference = SegmentFilter.difference(combined);
		SegmentSet differenceReversed = SegmentFilter.differenceReversed(combined);
		SegmentSet xor = SegmentFilter.xor(combined);

		visualizeSegmentSet(group, union, new Vector2d(300, 100));
		visualizeSegmentSet(group, intersection, new Vector2d(300, 0));
		visualizeSegmentSet(group, difference, new Vector2d(450, 100));
		visualizeSegmentSet(group, differenceReversed, new Vector2d(450, 0));
		visualizeSegmentSet(group, xor, new Vector2d(600, 100));

		visualizeFinalPolygon(group, SegmentChainer.chainSegments(union), new Vector2d(300, 100));
		visualizeFinalPolygon(group, SegmentChainer.chainSegments(intersection), new Vector2d(300, 0));
		visualizeFinalPolygon(group, SegmentChainer.chainSegments(difference), new Vector2d(450, 100));
		visualizeFinalPolygon(group, SegmentChainer.chainSegments(differenceReversed), new Vector2d(450, 0));
		visualizeFinalPolygon(group, SegmentChainer.chainSegments(xor), new Vector2d(600, 100));

		// create a scene
		Scene scene = new Scene(group, 900, 300);

		// set the scene
		stage.setScene(scene);

		stage.show();
	}

	private void visualizeSegmentSet(Pane group, SegmentSet c, Vector2d offset) {
		for (AnnotatedSegment segment : c.getSegments()) {
			visualizeSegment(group, segment, offset);
		}
	}

	private void visualizePolygon(Pane group, Color color, double[] pointCoordinates) {
		Polygon polygon = new Polygon(pointCoordinates);
		Paint fillColor = Color.color(color.getRed(), color.getGreen(), color.getBlue(), 0.25);
		polygon.setFill(fillColor);
		polygon.setStroke(color);
		group.getChildren().add(polygon);
	}

	private void visualizeFinalPolygon(Pane group, com.polyplay.Polygon finalPolygon, Vector2d offset) {
		Color colors[] = new Color[] { Color.ORANGE, Color.BLUE, Color.YELLOW, Color.PINK };
		int colorIndex = 0;
		for (Region r : finalPolygon.getRegions()) {
			ArrayList<Point> points = r.getPoints();

			double pointCoordinates[] = new double[points.size() * 2];
			int i = 0;
			for (Point p : points) {
				pointCoordinates[i++] = p.x() + offset.x();
				pointCoordinates[i++] = p.y() + offset.y();
			}
			Polygon polygon = new Polygon(pointCoordinates);
			Color color = colors[colorIndex];
			colorIndex = (colorIndex + 1) % colors.length;
			Paint fillColor = Color.color(color.getRed(), color.getGreen(), color.getBlue(), 0.5);
			polygon.setFill(fillColor);
			group.getChildren().add(polygon);
		}
	}

	private void visualizeSegment(Pane group, AnnotatedSegment segment, Vector2d offset) {
		Point start = segment.getStart();
		Point end = segment.getEnd();
		int colorIndex = (segment.getPrimary().above ? 1 : 0) + (segment.getPrimary().below ? 2 : 0);

		Line line = new Line(start.x() + offset.x(), start.y() + offset.y(), end.x() + offset.x(),
				end.y() + offset.y());
		line.setStroke(lineColors[colorIndex]);
		line.setStrokeWidth(2.5);
		FillInfo secondary = segment.getSecondary();
		if (secondary.above != null && secondary.below != null && (secondary.above || secondary.below)) {
			line.setStrokeLineCap(StrokeLineCap.BUTT);
			line.getStrokeDashArray().addAll(secondary.above ? 3d : 8d, secondary.below ? 8d : 3d);
		}

		group.getChildren().add(line);
	}

	private SegmentSet buildSegmentSet(double[] pointCoordinates) {
		SegmentSet segments = new SegmentSet();
		ArrayList<Point> points = new ArrayList<Point>();
		for (int i = 0; i + 1 < pointCoordinates.length; i += 2) {
			points.add(new Point(pointCoordinates[i], pointCoordinates[i + 1]));
		}
		Region region = new Region();
		region.setPoints(points);
		segments.setSegments(region.getSegments());
		return segments;
	}

	public static void main(String args[]) {
		// launch the application
		launch(args);
	}
}
