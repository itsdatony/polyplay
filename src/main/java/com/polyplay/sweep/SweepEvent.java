// The MIT License (MIT)
//
// Copyright (c) 2019-2020 Kyle Hawk
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package com.polyplay.sweep;

import java.util.ArrayList;

import com.polyplay.AnnotatedSegment;
import com.polyplay.Point;

public class SweepEvent {
	public SweepEvent(AnnotatedSegment segment, boolean isStart, boolean isPrimary) {
		this.segment = segment;
		this.isStart = isStart;
		this.isPrimary = isPrimary;
	}

	public boolean isStart() {
		return isStart;
	}

	public void setStart(boolean isStart) {
		this.isStart = isStart;
	}

	public boolean isPrimary() {
		return isPrimary;
	}

	public void setPrimary(boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	public AnnotatedSegment getSegment() {
		return segment;
	}

	public void setSegment(AnnotatedSegment segment) {
		this.segment = segment;
	}

	public SweepEvent getPairedEvent() {
		return pairedEvent;
	}

	public void setPairedEvent(SweepEvent pairedEvent) {
		this.pairedEvent = pairedEvent;
	}

	public Point getPoint() {
		return isStart ? segment.getStart() : segment.getEnd();
	}

	public Point getPairedPoint() {
		return isStart ? segment.getEnd() : segment.getStart();
	}

	@Override
	public String toString() {
		ArrayList<String> parts = new ArrayList<String>();
		parts.add(isPrimary ? "P" : "S");
		parts.add(":");
		parts.add(isStart ? "S" : "E");
		parts.add(" ");
		parts.add(Integer.toString(segment.getId()));
		if (isStart) {
			parts.add(segment.getStart().toString());
			parts.add(" -> ");
			parts.add(segment.getEnd().toString());
		} else {
			parts.add(segment.getEnd().toString());
			parts.add(" <- ");
			parts.add(segment.getStart().toString());
		}
		if (segment.getPrimary().above != null) {
			parts.add(" P");
			parts.add(segment.getPrimary().toString());
		}
		if (segment.getSecondary().above != null) {
			parts.add(" S");
			parts.add(segment.getSecondary().toString());
		}

		return String.join("", parts);
	}

	private boolean isStart, isPrimary;
	private AnnotatedSegment segment;
	private SweepEvent pairedEvent;
}
