// The MIT License (MIT)
//
// Copyright (c) 2019-2020 Kyle Hawk
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package com.polyplay.chain;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;

import com.polyplay.Point;
import com.polyplay.Region;

public class PointChain {
	public PointChain(Point start, Point end) {
		points.add(start);
		points.add(end);
	}

	// TODO Handle collinearity on wrap around

	public void addTrailingPoint(Point p) {
		Iterator<Point> descendingIterator = points.descendingIterator();
		Point chainEnd = descendingIterator.next();
		Point secondToLast = descendingIterator.next();

		if (Point.areCollinear(secondToLast, chainEnd, p)) {
			// Old last point is a useless subdivision of a straight line
			points.pollLast();
		}

		Point chainStart = points.peekFirst();
		if (p.compareTo(chainStart) == 0) {
			// We've looped back around
			isClosed = true;
			return;
		}

		points.addLast(p);
	}

	public void addLeadingPoint(Point p) {
		Iterator<Point> iterator = points.iterator();
		Point chainStart = iterator.next();
		Point second = iterator.next();

		if (Point.areCollinear(p, chainStart, second)) {
			// Old first point is a useless subdivision of a straight line
			points.pollFirst();
		}

		Point chainEnd = points.peekLast();
		if (p.compareTo(chainEnd) == 0) {
			// We've looped back around
			isClosed = true;
			return;
		}

		points.addFirst(p);
	}

	public void linkTrailingChain(PointChain postfixChain) {
		Iterator<Point> descendingIterator = points.descendingIterator();
		Point chainEnd = descendingIterator.next();
		Point secondToLast = descendingIterator.next();

		if (Point.areCollinear(secondToLast, chainEnd, postfixChain.getStart())) {
			// Old last point is a useless subdivision of a straight line
			points.pollLast();
		}

		points.addAll(postfixChain.points);

		Iterator<Point> iterator = points.iterator();
		Point chainStart = iterator.next();
		descendingIterator = points.descendingIterator();
		chainEnd = descendingIterator.next();
		if (chainEnd.compareTo(chainStart) == 0) {
			// We've looped back around
			isClosed = true;

			secondToLast = descendingIterator.next();
			Point second = iterator.next();

			// Remove duplicate point
			points.pollLast();
			if (Point.areCollinear(secondToLast, chainStart, second)) {
				// Old second to last point forms a straight line with the first segment
				points.pollLast();
			}
		}
	}

	public Point getStart() {
		return points.peekFirst();
	}

	public Point getEnd() {
		return points.peekLast();
	}

	public boolean isClosed() {
		return isClosed;
	}

	public Region getAsRegion() {
		Region output = new Region();
		output.setPoints(new ArrayList<Point>(points));
		return output;
	}

	private ArrayDeque<Point> points = new ArrayDeque<Point>();
	private boolean isClosed = false;
}
