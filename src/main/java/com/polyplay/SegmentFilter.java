// The MIT License (MIT)
//
// Copyright (c) 2019-2020 Kyle Hawk
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package com.polyplay;

import java.util.ArrayList;

public class SegmentFilter {

	private static enum Option {
		TRASH, ABOVE, BELOW
	}

	private static SegmentSet filter(SegmentSet input, Option mapping[]) {
		ArrayList<AnnotatedSegment> outputSegments = new ArrayList<AnnotatedSegment>();
		for (AnnotatedSegment segment : input.getSegments()) {
			int index = 0;
			if (segment.getPrimary().above) {
				index += 8;
			}
			if (segment.getPrimary().below) {
				index += 4;
			}
			if (segment.getSecondary().above != null && segment.getSecondary().above) {
				index += 2;
			}
			if (segment.getSecondary().below != null && segment.getSecondary().below) {
				index += 1;
			}
			Option filteredMapping = mapping[index];
			if (filteredMapping == Option.TRASH) {
				continue;
			}
			AnnotatedSegment filteredSegment = new AnnotatedSegment(segment);
			filteredSegment.getPrimary().above = filteredMapping == Option.ABOVE;
			filteredSegment.getPrimary().below = filteredMapping == Option.BELOW;
			outputSegments.add(filteredSegment);
		}
		SegmentSet output = new SegmentSet();
		output.setSegments(outputSegments);
		return output;
	}

	private static final Option[] unionOptions = new Option[] { //
			Option.TRASH, Option.BELOW, Option.ABOVE, Option.TRASH, //
			Option.BELOW, Option.BELOW, Option.TRASH, Option.TRASH, //
			Option.ABOVE, Option.TRASH, Option.ABOVE, Option.TRASH, //
			Option.TRASH, Option.TRASH, Option.TRASH, Option.TRASH };
	private static final Option[] xorOptions = new Option[] { //
			Option.TRASH, Option.BELOW, Option.ABOVE, Option.TRASH, //
			Option.BELOW, Option.TRASH, Option.TRASH, Option.ABOVE, //
			Option.ABOVE, Option.TRASH, Option.TRASH, Option.BELOW, //
			Option.TRASH, Option.ABOVE, Option.BELOW, Option.TRASH };
	private static final Option[] differenceReversedOptions = new Option[] { //
			Option.TRASH, Option.BELOW, Option.ABOVE, Option.TRASH, //
			Option.TRASH, Option.TRASH, Option.ABOVE, Option.ABOVE, //
			Option.TRASH, Option.BELOW, Option.TRASH, Option.BELOW, //
			Option.TRASH, Option.TRASH, Option.TRASH, Option.TRASH };
	private static final Option[] differenceOptions = new Option[] { //
			Option.TRASH, Option.TRASH, Option.TRASH, Option.TRASH, //
			Option.BELOW, Option.TRASH, Option.BELOW, Option.TRASH, //
			Option.ABOVE, Option.ABOVE, Option.TRASH, Option.TRASH, //
			Option.TRASH, Option.ABOVE, Option.BELOW, Option.TRASH };
	private static final Option[] intersectOptions = new Option[] { //
			Option.TRASH, Option.TRASH, Option.TRASH, Option.TRASH, //
			Option.TRASH, Option.BELOW, Option.TRASH, Option.BELOW, //
			Option.TRASH, Option.TRASH, Option.ABOVE, Option.ABOVE, //
			Option.TRASH, Option.BELOW, Option.ABOVE, Option.TRASH };

	public static SegmentSet union(SegmentSet input) {
		return filter(input, unionOptions);
	}

	public static SegmentSet intersect(SegmentSet input) {
		return filter(input, intersectOptions);
	}

	public static SegmentSet difference(SegmentSet input) {
		return filter(input, differenceOptions);
	}

	public static SegmentSet differenceReversed(SegmentSet input) {
		return filter(input, differenceReversedOptions);
	}

	public static SegmentSet xor(SegmentSet input) {
		return filter(input, xorOptions);
	}
}
