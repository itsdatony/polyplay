// The MIT License (MIT)
//
// Copyright (c) 2019-2020 Kyle Hawk
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package com.polyplay.chain;

import java.util.ArrayList;
import java.util.TreeMap;

import com.polyplay.AnnotatedSegment;
import com.polyplay.Point;
import com.polyplay.Polygon;
import com.polyplay.Region;
import com.polyplay.SegmentSet;

public class SegmentChainer {
	public static Polygon chainSegments(SegmentSet set) {
		TreeMap<Point, PointChain> starts = new TreeMap<Point, PointChain>();
		TreeMap<Point, PointChain> ends = new TreeMap<Point, PointChain>();
		ArrayList<Region> regions = new ArrayList<Region>();

		for (AnnotatedSegment segment : set.getSegments()) {
			// Maintain all chains in CCW => inside order to halve comparisons
			Point newStart, newEnd;
			if (segment.getPrimary().above) {
				newStart = segment.getStart();
				newEnd = segment.getEnd();
			} else {
				newStart = segment.getEnd();
				newEnd = segment.getStart();
			}

			PointChain prefixChain = ends.get(newStart);
			if (prefixChain != null) {
				ends.remove(prefixChain.getEnd());
				prefixChain.addTrailingPoint(newEnd);

				if (!prefixChain.isClosed()) {
					PointChain postfixChain = starts.get(newEnd);
					if (postfixChain != null) {
						// Remove postfix from consideration and append it to prefix
						starts.remove(postfixChain.getStart());
						ends.remove(postfixChain.getEnd());
						prefixChain.linkTrailingChain(postfixChain);
					}
				}

				if (prefixChain.isClosed()) {
					regions.add(prefixChain.getAsRegion());
					starts.remove(prefixChain.getStart());
				} else {
					ends.put(prefixChain.getEnd(), prefixChain);
				}
				continue;
			}

			PointChain postfixChain = starts.get(newEnd);
			if (postfixChain != null) {
				starts.remove(postfixChain.getStart());
				postfixChain.addLeadingPoint(newStart);

				if (postfixChain.isClosed()) {
					regions.add(postfixChain.getAsRegion());
					ends.remove(postfixChain.getEnd());
				} else {
					// Note that we've already eliminated the possibility of a second match here
					starts.put(newStart, postfixChain);
				}
				continue;
			}

			PointChain newChain = new PointChain(newStart, newEnd);
			starts.put(newStart, newChain);
			ends.put(newEnd, newChain);
		}

		Polygon output = new Polygon();
		output.setRegions(regions);
		output.setInverted(set.isInverted());
		return output;
	}
}
