// The MIT License (MIT)
//
// Copyright (c) 2019-2020 Kyle Hawk
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package com.polyplay;

import java.util.ArrayList;

public class AnnotatedSegment extends Segment {
	static private int currentId = 0;

	public AnnotatedSegment(Segment other) {
		super(other.getStart(), other.getEnd());
		id = currentId++;
	}

	public AnnotatedSegment(Point a, Point b) {
		super(a, b);
		id = currentId++;
	}

	public AnnotatedSegment(AnnotatedSegment other) {
		super(other.getStart(), other.getEnd());
		if (other.primary.above != null) {
			primary.above = Boolean.valueOf(other.primary.above);
			primary.below = Boolean.valueOf(other.primary.below);
		}
	}

	public static class FillInfo {
		public Boolean above = null, below = null;

		@Override
		public String toString() {
			return String.join("", "(", above ? "T" : "F", ",", below ? "T" : "F", ")");
		}
	}

	private FillInfo primary = new FillInfo(), secondary = new FillInfo();
	private int id;

	public FillInfo getPrimary() {
		return primary;
	}

	public void setPrimary(FillInfo primary) {
		this.primary = primary;
	}

	public FillInfo getSecondary() {
		return secondary;
	}

	public void setSecondary(FillInfo secondary) {
		this.secondary = secondary;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		ArrayList<String> parts = new ArrayList<String>();
		parts.add(Integer.toString(getId()));
		parts.add(getStart().toString());
		parts.add(" -> ");
		parts.add(getEnd().toString());
		if (getPrimary().above != null) {
			parts.add(" P");
			parts.add(getPrimary().toString());
		}
		if (getSecondary().above != null) {
			parts.add(" S");
			parts.add(getSecondary().toString());
		}

		return String.join("", parts);
	}
}
