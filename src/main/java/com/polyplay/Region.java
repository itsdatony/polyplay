// The MIT License (MIT)
//
// Copyright (c) 2019-2020 Kyle Hawk
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package com.polyplay;

import java.util.ArrayList;

public class Region {
	public ArrayList<Point> getPoints() {
		return points;
	}

	public void setPoints(ArrayList<Point> points) {
		this.points = points;
	}

	public boolean containsPoint(Point p) {
		if (points.isEmpty()) {
			return false;
		}

		boolean inside = false;
		Point previous = points.get(points.size() - 1);
		for (Point current : points) {
			if ((current.y() - p.y() > Config.epsilon) != (previous.y() - p.y() > Config.epsilon)) {
				// p is in general horizontal region between previous and current
				if ((previous.x() - current.x()) * (p.y() - current.y()) / (previous.y() - current.y()) + current.x()
						- p.x() > Config.epsilon) {
					// x is to the right of the segment defined by previous->current, count an
					// intersection with our ray
					inside = !inside;
				}

			}
			previous = current;
		}
		return inside;
	}

	public ArrayList<AnnotatedSegment> getSegments() {
		ArrayList<AnnotatedSegment> output = new ArrayList<AnnotatedSegment>();
		if (points.size() < 2) {
			return output;
		}
		Point previous = points.get(points.size() - 1);
		for (Point current : points) {
			output.add(new AnnotatedSegment(previous, current));
			previous = current;
		}
		return output;
	}

	private ArrayList<Point> points;
}
