// The MIT License (MIT)
//
// Copyright (c) 2019-2020 Kyle Hawk
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package com.polyplay.sweep;

import java.util.ArrayList;

import com.polyplay.AnnotatedSegment;
import com.polyplay.AnnotatedSegment.FillInfo;
import com.polyplay.Config;
import com.polyplay.Point;
import com.polyplay.SegmentSet;
import com.polyplay.Vector2d;

public class SegmentAnnotater {
	public static SegmentSet getSegments(SegmentSet primary, SegmentSet secondary) {
		SweepEventQueue queue = new SweepEventQueue();
		StatusStack status = new StatusStack();

		if (primary != null) {
			for (AnnotatedSegment segment : primary.getSegments()) {
				queue.addSegment(segment, true);
			}
		}
		if (secondary != null) {
			for (AnnotatedSegment segment : secondary.getSegments()) {
				queue.addSegment(segment, false);
			}
		}
		boolean primaryInverted = primary != null && primary.isInverted();
		boolean secondaryInverted = secondary != null && secondary.isInverted();

		ArrayList<AnnotatedSegment> segments = new ArrayList<AnnotatedSegment>();
		while (!queue.isEmpty()) {
			SweepEvent event = queue.peekHead();
			if (event.isStart()) {
				handleStartEvent(status, queue, secondary == null, primaryInverted, secondaryInverted, event);
			} else {
				handleEndEvent(status, queue, segments, event);
			}
		}

		SegmentSet output = new SegmentSet();
		output.setSegments(segments);
		return output;
	}

	private static void handleStartEvent(StatusStack status, SweepEventQueue queue, boolean selfIntersection,
			boolean primaryInverted, boolean secondaryInverted, SweepEvent event) {
		SweepEvent intersectionEvent = null;
		SweepEvent below = status.getNext(event);
		SweepEvent above = status.getPrevious(event);
		if (above != null) {
			intersectionEvent = handleIntersection(queue, event, above);
		}
		if (intersectionEvent == null && below != null) {
			intersectionEvent = handleIntersection(queue, event, below);
		}

		FillInfo eventPrimaryFillInfo = event.getSegment().getPrimary();

		if (intersectionEvent != null) {
			if (selfIntersection) {
				boolean toggle = eventPrimaryFillInfo.below == null
						|| eventPrimaryFillInfo.above != eventPrimaryFillInfo.below;
				if (toggle) {
					FillInfo newPrimaryFillInfo = intersectionEvent.getSegment().getPrimary();
					newPrimaryFillInfo.above = !newPrimaryFillInfo.above;
				}
			} else {
				intersectionEvent.getSegment().setSecondary(eventPrimaryFillInfo);
			}
			queue.removePairedEvents(event);
		}

		if (queue.peekHead() != event) {
			return;
		}

		if (selfIntersection) {
			boolean toggle = eventPrimaryFillInfo.below == null
					|| eventPrimaryFillInfo.above != eventPrimaryFillInfo.below;
			if (below == null) {
				eventPrimaryFillInfo.below = primaryInverted;
			} else {
				eventPrimaryFillInfo.below = below.getSegment().getPrimary().above;
			}
			if (toggle) {
				eventPrimaryFillInfo.above = !eventPrimaryFillInfo.below;
			} else {
				eventPrimaryFillInfo.above = eventPrimaryFillInfo.below;
			}
		} else {
			FillInfo secondaryFillInfo = event.getSegment().getSecondary();
			if (secondaryFillInfo.below == null && secondaryFillInfo.above == null) {
				boolean insideOther;
				if (below == null) {
					insideOther = event.isPrimary() ? secondaryInverted : primaryInverted;
				} else if (event.isPrimary() == below.isPrimary()) {
					insideOther = below.getSegment().getSecondary().above;
				} else {
					insideOther = below.getSegment().getPrimary().above;
				}
				secondaryFillInfo.above = insideOther;
				secondaryFillInfo.below = insideOther;
			}
		}
		queue.popHead();
		status.add(event);
	}

	private static void handleEndEvent(StatusStack status, SweepEventQueue queue, ArrayList<AnnotatedSegment> segments,
			SweepEvent event) {
		SweepEvent prev = status.getPrevious(event);
		SweepEvent next = status.getNext(event);
		if (prev != null && next != null) {
			handleIntersection(queue, prev, next);
		}
		status.remove(event);

		AnnotatedSegment segment = event.getSegment();
		if (!event.isPrimary()) {
			// Swap segment fill info
			AnnotatedSegment.FillInfo oldPrimaryFillInfo = segment.getPrimary();
			segment.setPrimary(segment.getSecondary());
			segment.setSecondary(oldPrimaryFillInfo);
		}
		segments.add(segment);
		queue.popHead();
	}

	private static SweepEvent handleIntersection(SweepEventQueue queue, SweepEvent a, SweepEvent b) {
		AnnotatedSegment segmentA = a.getSegment();
		AnnotatedSegment segmentB = b.getSegment();
		Point startA = segmentA.getStart();
		Point endA = segmentA.getEnd();
		Point startB = segmentB.getStart();
		Point endB = segmentB.getEnd();
		Vector2d vectorA = new Vector2d(startA, endA);
		Vector2d vectorB = new Vector2d(startB, endB);

		double axb = vectorA.getSignedAreaWith(vectorB);
		if (Math.abs(axb) < Config.epsilon) {
			// Segments are parallel
			if (!Point.areCollinear(startA, endA, startB)) {
				// Segments don't coincide at all
				return null;
			}
			if (startA.equals(endB) || startB.equals(endA)) {
				// Segments line up end to end (no significant intersection)
				return null;
			}

			boolean startsSame = startA.equals(startB);
			boolean endsSame = endA.equals(endB);
			if (startsSame && endsSame) {
				// Segments are the same, need to merge these later
				return b;
			}
			boolean endAisInB = !endsSame && segmentB.isInBounds(endA);
			if (startsSame) {
				if (endAisInB) {
					queue.divideStartEventSegment(b, endA);
				} else {
					queue.divideStartEventSegment(a, endB);
				}
				return b;
			} else if (segmentB.isInBounds(startA)) {
				if (!endsSame) {
					if (endAisInB) {
						queue.divideStartEventSegment(b, endA);
					} else {
						queue.divideStartEventSegment(a, endB);
					}
				}
				queue.divideStartEventSegment(b, startA);
				return null;
			}
		}

		Vector2d vectorB2A = new Vector2d(startB, startA);
		double s = vectorB.getSignedAreaWith(vectorB2A) / axb;
		double t = vectorA.getSignedAreaWith(vectorB2A) / axb;

		boolean inA = s >= Config.epsilon && s - 1.0 <= -Config.epsilon;
		boolean inB = t >= Config.epsilon && t - 1.0 <= -Config.epsilon;
		if (inA && inB) {
			Point intersection = new Point(startA.x() + s * vectorA.x(), startA.y() + s * vectorA.y());
			queue.divideStartEventSegment(a, intersection);
			queue.divideStartEventSegment(b, intersection);
			return null;
		} else if (inA) {
			// Handle endpoint intersections with B, if they exist
			if (Math.abs(t) < Config.epsilon) {
				queue.divideStartEventSegment(a, startB);
			} else if (Math.abs(t - 1.0) < Config.epsilon) {
				queue.divideStartEventSegment(a, endB);
			}
		} else if (inB) {
			// Handle endpoint intersections with A, if they exist
			if (Math.abs(s) < Config.epsilon) {
				queue.divideStartEventSegment(b, startA);
			} else if (Math.abs(s - 1.0) < Config.epsilon) {
				queue.divideStartEventSegment(b, endA);
			}
		}
		return null;
	}
}
