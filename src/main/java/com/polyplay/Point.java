// The MIT License (MIT)
//
// Copyright (c) 2019-2020 Kyle Hawk
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package com.polyplay;

import java.util.Arrays;

public class Point implements Comparable<Point> {

	public Point() {
	}

	public Point(double x, double y) {
		setX(x);
		setY(y);
	}

	public void setX(double x) {
		coordinates[0] = x;
	}

	public void setY(double y) {
		coordinates[1] = y;
	}

	public double x() {
		return coordinates[0];
	}

	public double y() {
		return coordinates[1];
	}

	public boolean hasSameX(Point other) {
		return Math.abs(other.x() - x()) < Config.epsilon;
	}

	public boolean hasSameY(Point other) {
		return Math.abs(other.y() - y()) < Config.epsilon;
	}

	public static boolean areCollinear(Point a, Point b, Point c) {
		Vector2d d1 = new Vector2d(b, a);
		Vector2d d2 = new Vector2d(c, b);
		return d1.getAreaWith(d2) < Config.epsilon;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point other = (Point) obj;
		return hasSameX(other) && hasSameY(other);
	}

	@Override
	public int compareTo(Point other) {
		if (!hasSameX(other)) {
			return Double.compare(x(), other.x());
		}
		if (hasSameY(other)) {
			return 0;
		}
		return Double.compare(y(), other.y());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(coordinates);
		return result;
	}

	@Override
	public String toString() {
		String[] parts = { "{", Double.toString(this.x()), ", ", Double.toString(this.y()), "}" };
		return String.join("", parts);
	}

	private double coordinates[] = new double[2];

}
